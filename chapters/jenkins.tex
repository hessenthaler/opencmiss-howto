%===============================================================================
%
\section{Jenkins}\label{jenkins-sec}
%
We use Jenkins for automated testing. Here, we provide some detail about what we
test (at the moment), why we test and how we do it.
%
%===============================================================================
%
\subsection{Version control}\label{version-control-subsec}
%
The common code base for \iron\ developers at the University of Stuttgart
is hosted on \github\footnote{\url{https://github.org/cbm-software/iron}}
and is a direct fork from the main \opencmiss\ \github\ organization.

In addition to the existing branches of the fork, there are currently the
following branches:
%
\begin{itemize}
    \item{dihu-project: Common code base for all developers associated with
        the DiHu-project.}
    \item{-}
\end{itemize}
%
%===============================================================================
%
\subsection{Continuous integration}\label{continuous-integration-subsec}
%
There are many good online resources, discussing the principles of continuous
integration (CI).
A good starting point is, for example, Martin Fowler's webpage:
%
\begin{itemize}
    \item{\url{http://www.martinfowler.com/articles/continuousIntegration.html}}
\end{itemize}
%
The fundamental idea of CI is that new features are continuously integrated and
tested to enhance development processes and continuous delivery as well as
continuous deployment.

We make use of some of the principles of CI to simplify development pipelines
and code sharing between \opencmiss\ developers at the University of Stuttgart.
Here are some important aspects:
%
\begin{itemize}
    \item{Divide your implementation tasks into smaller tasks!}
    \item{Commit often!}
    \item{Build (use \emph{make clean} before building!)
        and test (use the target \emph{keytests} for that purpose) before committing!}
    \item{Keep up-to-date with the upstream repositories!}
    \item{Integrate your code as often as possible (pull requests)!}
    \item{Trigger pull requests only if your code builds, runs and gives the expected results!}
    \item{If there is a failed test, there is nothing more imporant than fixing it!}
    \item{If you don't understand the expressions
        \textit{commit}/\textit{pull request}/\textit{etc.}\ read up on git version control!
        \footnote{\url{https://www.atlassian.com/git}}}
\end{itemize}
%
%===============================================================================
%
\subsubsection{Workflow for DiHu project developers}
%
Follow the workflow:
%
\begin{enumerate}
    \item{make changes in working copy
        \label{workflow-changes-enum}}
    \item{if code does not build, go back to \ref{workflow-changes-enum}
        \label{workflow-build-enum}}
    \item{if there is a single failed test, go back to \ref{workflow-changes-enum}
        \label{workflow-tests-enum}}
    \item{if there are upstream changes in the code base,
        merge those and go back to \ref{workflow-build-enum}
        \label{workflow-merge-upstream-enum}}
    \item{now, commit and push changes to own github fork}
    \item{execute the build pipeline for your github fork, see Section~\ref{automated-testing-sec}
        (eventually, this will happen automatically; for now, you will have to trigger it manually)}
    \item{if code does not compile on \lead\ or if there was a single failed test,
        go back to \ref{workflow-changes-enum} and fix it
        \label{workflow-failed-test-lead-enum}}
    \item{pull-request to get your changes into the common code base}
    \item{accept pull request on github}
    \item{execute the build pipeline for the common code base (name \emph{iron\_dihu-project})
        (again, this will be automated at some point)}
    \item{if code does not compile on \lead\ or if there was a single failed test,
        go back to \ref{workflow-changes-enum} and fix it}
    \item{begin with your next implementation task,
        i.e.\ go back to \ref{workflow-changes-enum}}
\end{enumerate}
%
%===============================================================================
%
\subsubsection{Automated testing}\label{automated-testing-sec}
%
We use Jenkins to run automated builds and tests on \lead. We follow
a demand-driven approach, i.e., we test if there were code changes or if a
developer requests an automated test. Thus, we test if:
%
\begin{itemize}
    \item{\iron\ builds,}
    \item{provided \iron\ tests (namely \emph{keytests}) pass,}
    \item{our own set of examples build, run and give expected results (to be implemented).}
\end{itemize}
%
What we do not test at the moment:
%
\begin{itemize}
    \item{\dependencies}
\end{itemize}
%
%===============================================================================
%
\clearpage
%
\subsubsection{Trigger automated testing pipeline}\label{trigger-automated-testing-pipeline-sec}
%
Here's how to trigger an automated test for the \emph{iron\_dihu-project} code base:
%
\begin{itemize}
    \item{login to \lead}
    \item{make sure the firefox module is loaded}
    \item{firefox \url{http://leadx06:8083/jenkins}}
    \item{login (if you don't have login credentials, ask an administrator)}
    \item{click on the item \emph{iron\_dihu-project}}
    \item{click on \emph{Build with Parameters}}
    \item{click on \emph{Build}}
\end{itemize}
%
Your build might not start immediately. This is because the pipeline is executed
by a Jenkins slave on the login node \emph{lead} and the slave is started on
demand. Typical runtime for the pipeline is approximately 5 minutes.

The pipeline will prepare, configure, build, test and parse test results
(and cleanup if all previous stages finished successfully).
Success or failure of the different pipeline stages are indicated in green and
red, respectively.\\[3ex]

{\color{gray}{
Alternatively, you can login to \lead, and browse to
\url{http://leadx06:8083/jenkins/job/iron_dihu-project/buildWithParameters?token=TOKEN_NAME}
to trigger a build, where \emph{TOKEN\_NAME} is a token known by \opencmiss\
developers in Stuttgart (ask an administrator). However, this will only trigger
the build and tests but not show you any results. You will have to login for
that!
}}\\[3ex]
%
%===============================================================================
%
\subsubsection{Add new testing pipeline}\label{new-testing-pipeline-sec}
%
Simply add a \emph{New Item} at the landing page of the Jenkins server, enter
a name and configure your own pipeline (check the docs) or copy an existing
pipeline (that you can then modify).
%
%===============================================================================
%
\clearpage
%
\subsubsection{Some documentation}\label{jenkins-doc-sec}
%
There is a user \emph{jenkins} on lead which is used to administrate anything
Jenkins server-related. We use Java 1.8.0\_111 at \textasciitilde/software/java.
The jenkins.jar file is here: \textasciitilde/software/jenkins (this folder also
contains some scripts for convenience. Please use them, since it makes it easier
to kill the Jenkins server).

Jenkins is started on leadx06 by the script \emph{start\_jenkins.sh},
which executes the commands
%
\begin{verbatim}
    $ DATE=`date +%Y-%m-%d:%H:%M:%S`
    $ nohup java -Xmn128M -Xms4096M -Xmx8192M -jar jenkins.war \
    $   --httpPort=8083 --prefix=/jenkins \
    $   --argumentsRealm.passwd.jenkins=init \
    $   --argumentsRealm.roles.jenkins=admin \
    $   > $DATE.jenkins.log 2>&1&
    $ echo $! > save_pid.txt
\end{verbatim}
%
which limits memory usage (see Java
docs\footnote{\url{https://docs.oracle.com/javase/8/docs/technotes/tools/windows/java.html}}),
sets the port for the weblogin, stores the PID of the process, etc.
There will be logs for each instance of the Jenkins server, i.e., there will be
a new log once the Jenkins server process is killed and restarted.

We kill the Jenkins server based on its PID on start-up
%
\begin{verbatim}
    $ kill -9 `cat save_pid.txt`
\end{verbatim}
%
and administrate it through the weblogin at
%
\begin{verbatim}
    $ firefox leadx06:8083/jenkins &
\end{verbatim}
%
This will work from \lead\ only - please access the Jenkins server from the
login node.
Currently, there are user accounts for \emph{hessenthaler, klotz, jenkins}.
Ask one of these admins for access.
%
%===============================================================================
